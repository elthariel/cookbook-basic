# Basic cookbook

This is the cookbook I use as a foundation for all my customer's
infrastructures. It handles the tasks you'll likely wants to handle on
any machine you manage:

- Simple user management with ssh keys configuration
- Sets the hostname to the node name
- Automated chef runs
- Unattended upgrades
- Fail2ban
- Firewall rules
- Useful software packages you want available everywhere (system
  monitoring, editors, build tools, ...)

# Usage

The cookbook attributes are namespaced under the `base` key. Depending
on your own personal style, you can either wrap this `basic` cookbook
in a `base` cookbook, or use the `basic` recipes in your roles.

In all the cases, you'll need to have the `basic::early` recipe first
in your run list and the `basic::late` recipe last in your run list.
