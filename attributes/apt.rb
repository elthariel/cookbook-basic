#
# APT related attributes
#

default['apt']['compile_time_update'] = true
default['apt']['unattended_upgrades']['enable'] = true
default['apt']['unattended_upgrades']['minimal_steps'] = true
default['apt']['unattended_upgrades']['allowed_origins'] = [
  '${distro_id}:${distro_codename}',
  '${distro_id}:${distro_codename}-security',
  '${distro_id}:${distro_codename}-updates',
]

case node['platform']
when 'debian'
  case node['platform_version']
  when /^9/
    default['apt']['unattended_upgrades']['allowed_origins'] += [
      'Debian:oldstable'
    ]
  end
end
