#
# Attributes for SSL generation
#

default['base']['cert']['enabled'] = false
default['base']['cert']['email'] = nil
default['base']['cert']['common_name'] = nil
default['base']['cert']['alt_names'] = []

default['base']['cert']['acme_wwwroot'] = '/var/lib/acme_wwwroot'
# Do we server acme request with our own tiny acme httpd
default['base']['cert']['acme_httpd'] = true

default['base']['cert']['path'] = '/etc/ssl/acme'

default['base']['cert']['owner'] = 'root'
default['base']['cert']['group'] = 'root'
default['base']['cert']['mode'] = 0751
