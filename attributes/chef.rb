#
# Chef related attributes
#

# Chef periodic runs
interval = 15 * 60
default['chef_client']['interval'] = interval
default['chef_client']['splay'] = interval / 2
default['chef_client']['systemd']['timeout'] = (interval * 0.9).to_i

# Sets the niceness of the chef process to reduce production impact
default['chef_client']['cron']['priority'] = 10

# Various paths
default['chef_client']['config']['trusted_certs_dir'] =
  '/etc/chef/trusted_certs'
default['ohai']['plugin_path'] =
  node['chef_client']['conf_dir'] + '/ohai/plugins'
