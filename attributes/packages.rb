#
# Packages to be install on all the boxes
#

# Install build-essential packages ?
default['base']['build_essential'] = true

# Default package groups
core_pkgs = %w{sudo file git mosh tree unzip}
debug_pkgs = %w{dnsutils htop iotop lsof strace iftop}
editor_pkgs = %w{vim}

case node['platform']
when 'ubuntu'
  core_pkgs << 'linux-tools-generic'
  case node['platform_version']
  when '18.04'
    editor_pkgs << 'emacs25-nox'
  else
    editor_pkgs << 'emacs24-nox'
  end
when 'debian'
  core_pkgs << 'linux-tools'
  core_pkgs << 'dirmngr'
  editor_pkgs << 'emacs24-nox'
end

default['base']['pkgs'] = {
  'core' => core_pkgs,
  'debug' => debug_pkgs,
  'editors' => editor_pkgs
}
