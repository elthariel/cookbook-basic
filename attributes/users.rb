#
# Simple but powerful system user management
#

default['base']['users'] = {}

# # unix_user_1 will get an account with sudo power on ALL the
# # machines
# default['base']['users']['unix_user_1'] = {
#   name: 'Full NAME',
#   email: 'unix_user_1@example.com',
#   shell: '/bin/bash',
#   keys: ['ssh-rsa XXX'],
#   tags: {
#     account: ['_all_'],
#     root: ['_all_']
#   }
# }

# # unix_user_2 will get an account on ALL the machines, but will only
# # get sudo power on node_name_1, node_name_2 and all the machine with
# # the chef node name matching the regexp /postgres_\d+/
# default['base']['users']['unix_user_2'] = {
#   name: 'Full NAME',
#   email: 'unix_user_2@example.com'
#   keys: ['ssh-rsa YYY'],
#   tags: {
#     account: ['_all_'],
#     root: ['node_name_1', 'node_name_2', /postgres_\d+/]
#   }
# }

# # unix_user_3 will get any existing account deleted
# default['base']['users']['unix_user_3'] = {
#   name: 'Full NAME',
#   email: 'unix_user_3@example.com'
#   keys: ['ssh-rsa ZZZ'],
#   tags: {
#     account: [],
#     root: []
#   }
# }
