# frozen_string_literal: true

# Here we monkey patch a few helpers methods in the node object.
# While this isn't usually considered a very nice way to extend the
# features of a software, the convenience seems worth it

class Chef
  #
  # Sharding primitives. Allowing to perform progressive roll out of
  # features or changes
  #
  class Node
    # Every node gets assigned a shard_id, which is computed from its
    # name. The shards count is configurable, but unless you have a
    # large number of machines (> 1k), you probably want to stick with
    # 100.
    def shard_id(shards = 100)
      Digest::MD5.hexdigest(name).to_i(16) % shards
    end

    # Returns true if the current node shard_id is below the given
    # threshold. This is used to do rolling deployment of features.
    #
    # Example:
    # ```ruby
    # # 50% of the box will run implem A, the rest implem B
    # if node.in_shard?(50)
    #   # Implementation A
    # else
    #   # Implementation B
    # end
    # ```
    def in_shard?(threshold, shards = 100)
      shard_id(shards) <= threshold
    end
  end
end
