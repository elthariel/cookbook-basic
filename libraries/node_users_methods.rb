# frozen_string_literal: true

def tag_valid_on_node_name?(name, patterns)
  patterns.each do |pattern|
    if pattern.is_a? String
      return true if pattern.to_s == '_all_'
      return true if pattern.to_s == name
    end
    return true if pattern.is_a?(Regexp) && pattern.match(name)
  end
  false
end

module Base
  # Helpers to handle system user management
  class UsersHelpers
    attr_reader :node

    def initialize(node)
      @node = node
    end

    def with_tag(tag_name)
      node['base']['users'].to_hash.select do |user_name, details|
        tags = details['tags']
        if tags && tags[tag_name.to_s]
          tag_valid_on_node_name?(node.name, tags[tag_name.to_s])
        else
          false
        end
      end
    end

    def without_tag(tag_name)
      node['base']['users'].to_hash.reject do |user_name, details|
        tags = details['tags']
        if tags && tags[tag_name.to_s]
          tag_valid_on_node_name?(node.name, tags[tag_name.to_s])
        else
          false
        end
      end
    end
  end
end

#
# Another monkey patch thing to help with user management
#
class Chef
  class Node
    def users
      Base::UsersHelpers.new(self)
    end
  end
end
