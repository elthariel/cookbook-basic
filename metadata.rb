name 'basic'
maintainer "Julien 'Lta' BALLET"
maintainer_email 'contact@lta.io'
license 'MIT'
description 'Foundation for your infrastructure'
version '0.1.0'
chef_version '>= 12.1' if respond_to?(:chef_version)

issues_url 'https://gitlab.com/elthariel/cookbook-base/issues'
source_url 'https://gitlab.com/elthariel/cookbook-base'

depends 'acme', '~> 4.1.0'
depends 'apt', '~> 7.0.0'
depends 'build-essential'
depends 'chef-client', '~> 11.2.0'
depends 'fail2ban'
depends 'hostname'
depends 'iptables'
depends 'user'
depends 'systemd'
