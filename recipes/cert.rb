#
# Cookbook Name:: basic
# Recipe:: ssl
#
# Copyright 2020, Julien \'Lta\' BALLET
#
# MIT License
#

if node['base']['cert']['enabled']
  include_recipe 'acme'

  if node['base']['cert']['email'].nil?
    Chef::Log.fatal "You need to provide an email address in " \
      "node['base']['cert']['email']"
  end
  if node['base']['cert']['common_name'].nil?
    Chef::Log.fatal "You need to provide a dns name in " \
      "node['base']['cert']['common_name']"
  end

  email = node['base']['cert']['email']
  cn = node['base']['cert']['common_name']
  dir = node['base']['cert']['path']
  acme_wwwroot = node['base']['cert']['acme_wwwroot']

  directory dir do
    owner node['base']['cert']['owner']
    group node['base']['cert']['group']
    mode node['base']['cert']['mode']

    action :create
  end

  directory acme_wwwroot do
    user 'nobody'
    group 'nogroup'
  end

  acme_selfsigned cn do
    crt "#{dir}/#{cn}.crt"
    key "#{dir}/#{cn}.key"

    owner node['base']['cert']['owner']
    group node['base']['cert']['group']
  end

  if node['base']['cert']['acme_httpd']
    chef_ruby = Gem.ruby
    acme_httpd = File.join(File.dirname(chef_ruby), 'acme-httpd')

    service 'acme_httpd' do
      ignore_failure true
      action :nothing
    end

    template acme_httpd do
      source 'tiny-acme-httpd.rb.erb'
      variables ruby_path: chef_ruby, wwwroot: acme_wwwroot

      mode 0755
      action :create

      notifies :restart, 'service[acme_httpd]'
    end

    systemd_service 'acme_httpd' do
      unit do
        description 'Tiny ruby webserver for acme auth'
        after 'network.target'
      end

      service do
        exec_start acme_httpd
        restart 'on-failure'
      end

      # service_user 'nobody'
      # service_group 'nogroup'

      action [:create, :enable, :start]
    end
  end

  acme_certificate cn do
    crt "#{dir}/#{cn}.crt"
    key "#{dir}/#{cn}.key"

    wwwroot acme_wwwroot

    alt_names node['base']['cert']['alt_names']
  end
end
