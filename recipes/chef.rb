#
# Cookbook:: base
# Recipe:: chef
#
# Copyright:: 2019, The Authors.
#
# Configures the chef periodic runs and minor chef related things

if node['init_package'] == 'systemd'
  node.default['chef_client']['systemd']['timer'] = true
end

include_recipe 'chef-client'
include_recipe 'chef-client::config'

cookbook_file '/etc/profile.d/add_chef_embedded_to_path.sh' do
  source 'add_chef_embedded_to_path.sh'
  owner 'root'
  group 'root'
  mode '0755'
  action :create
end
