#
# Cookbook:: base
# Recipe:: default
#
# Copyright:: 2019, The Authors.
#
# This cookbook has two entry points: `early` and `late`.  The first
# one should be (one of) the first run and the latter should be run
# last.
# The `default` recipe is an alias for the `early` recipe.

include_recipe '::early'
