#
# Cookbook:: base
# Recipe:: early
#
# Copyright:: 2019, The Authors.

#
# First and foremost, Set the machine hostname to something decent
# (i.e. the node name)
#
include_recipe 'hostname::default'

Chef::Log.info "[Sharding] I am in the shard #{node.shard_id} / 100"

if node['platform_family'] == 'debian'
  include_recipe 'apt::default'
  include_recipe 'apt::unattended-upgrades'
end

include_recipe '::chef'
include_recipe '::packages'
include_recipe '::users'
include_recipe '::cert'
