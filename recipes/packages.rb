#
# Cookbook:: base
# Recipe:: packages
#
# Copyright:: 2019, The Authors.
#
# Installs the base packages
#

#
# Standards compilers and development tools. While they are a security
# concern, they're very often needed to build ruby extensions and libraries
# required by deployed softwares
#
if node['base']['build_essential']
  build_essential 'build-essential' do
    compile_time true
  end
end

#
# Base packages for all hosts
#
node['base']['pkgs'].to_hash do |group, pkgs|
  package "pkgs_#{group}" do
    package_name pkgs
    action :upgrade
  end
end
