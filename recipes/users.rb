#
# Cookbook:: base
# Recipe:: users
#
# Copyright:: 2019, The Authors.
#
# Handle user management with ssh key support
#

users_with_account = node.users.with_tag('account')
users_without_account = node.users.without_tag('account')
users_with_root = node.users.with_tag('root')

usernames_with_account = users_with_account.map { |name, _| name }
usernames_without_account = users_without_account.map { |name, _| name }
usernames_with_root = users_with_root.map { |name, _| name }
Chef::Log.info("Users with account: #{usernames_with_account.join(', ')}")
Chef::Log.info("Users with root: #{usernames_with_root.join(', ')}")
Chef::Log.info("Users without account: #{usernames_without_account.join(', ')}")

users_with_account.each do |name, data|
  user_account name do
    ssh_keygen false
    ssh_keys data['keys']
    shell data['shell'] if data.key? 'shell'
    # XXX: Seems to be broken in the 'user' cookbook
    # groups data[:groups]
  end
end

users_without_account.each do |name, data|
  user_account name do
    action :remove
  end
end

template '/etc/sudoers.d/no_passwd_users' do
  source 'sudoers.erb'
  variables users: usernames_with_root
end
